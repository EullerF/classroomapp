import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "./src/loginScreen";
import HomePage from "./src/homePage";
import SignupScreen from "./src/signupScreen";
import SchedulesClassroom from "./src/schedulesClassroom";
import { enableScreens } from 'react-native-screens';
enableScreens();


const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ title: "Login" }}
        />
        <Stack.Screen
          name="HomePage"
          component={HomePage}
          options={{ title: "Classroom Reservation" }}
        />
        <Stack.Screen
          name="SignupScreen"
          component={SignupScreen}
          options={{ title: "Cadastre-se" }}
        />
        <Stack.Screen 
        name="SchedulesClassroom" 
        component={SchedulesClassroom} 
        options={{ title: "Agendamentos" }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
