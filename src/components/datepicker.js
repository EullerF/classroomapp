import React, { Component, useState } from "react";
import { View, Button, StyleSheet } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const DateTimePicket = ({ type, buttonTitle, dateKey, setSchedule }) => {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    if (type === "time") {
      // Extrair apenas hora e minuto
      const hour = date.getHours();
      const minute = date.getMinutes();

      // Formatar hora e minuto como desejado
      const formattedTime = `${hour}:${minute}`;

      // Atualizar o estado com a hora e o minuto formatados
      setSchedule((prevState) => ({
        ...prevState,
        [dateKey]: formattedTime,
      }));
    } else {
      const formattedDate = date.toISOString().split("T")[0];
      setSchedule((prevState) => ({
        ...prevState,
        [dateKey]: formattedDate,
      }));
    }
    hideDatePicker();
  };

  return (
    <View>
      <View style={styles.buttonWrapper}>
        <Button title={buttonTitle} onPress={showDatePicker} color="#0f3b50" />
      </View>
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode={type}
        locale="pt_BR"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
        // Ajuste o estilo do modal
        pickerContainerStyleIOS={{ backgroundColor: "#fff" }}
        textColor="#000" // Cor do texto
      />
    </View>
  );
};

const styles = StyleSheet.create({
  buttonWrapper: {
    width: 150,
    borderRadius: 5,
    overflow: "hidden",
  },
});
export default DateTimePicket;
