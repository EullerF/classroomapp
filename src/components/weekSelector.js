import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { format, addWeeks, startOfWeek, endOfWeek } from 'date-fns';

const WeekSelector = ({ onSelectWeek }) => {
  const [selectedWeek, setSelectedWeek] = useState(new Date());

  const handleSelectWeek = (weekStart,weekEnd) => {
    setSelectedWeek(weekStart);
    onSelectWeek(weekStart,weekEnd);
  };

  const generateWeeks = () => {
    const currentWeek = new Date();
    const weeks = [];

    for (let i = 0; i < 5; i++) { // Gera 5 semanas a partir da semana atual
      const start = startOfWeek(addWeeks(currentWeek, i));
      const end = endOfWeek(addWeeks(currentWeek, i));

      const formattedWeek = `${format(start, 'dd/MM')} - ${format(end, 'dd/MM')}`;
      weeks.push({ start, end, formattedWeek });
    }

    return weeks;
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Selecione a Semana:</Text>
      <View style={styles.weekButtonsContainer}>
        {generateWeeks().map((week, index) => (
          <TouchableOpacity
            key={index}
            style={[
              styles.weekButton,
              +selectedWeek === +week.start && styles.selectedWeekButton,
            ]}
            onPress={() => handleSelectWeek(week.start,week.end)}
          >
            <Text style={styles.weekText}>{week.formattedWeek}</Text>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginBottom: 20,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  weekButtonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%', // Ocupa toda a largura da tela
  },
  weekButton: {
    padding: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ccc',
    flex: 1, // Cada botão ocupa uma parte igual do espaço disponível
    marginHorizontal: 5,
  },
  selectedWeekButton: {
    backgroundColor: '#084d6e',
    borderColor: '#084d6e',
  },
  weekText: {
    fontSize: 14,
    color: '#000', // Cor do texto para semanas não selecionadas
    textAlign: 'center', // Centraliza o texto dentro do botão
  },
});

export default WeekSelector;
