import React, { useState } from "react";
import {
  View,
  Button,
  TextInput,
  StyleSheet,
  Text,
  Alert,
  TouchableOpacity,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import api from "./axios/axios";

const HomeScreen = () => {
  const navigation = useNavigation();
  const [credentials, setCredentials] = useState({ email: "", password: "" });

  const handleLogin = () => {
    // Exemplo de chamada para criar um usuário com os dados do formulário de login
    api
      .loginUser(credentials)
      .then((response) => {
        console.log(response.data); // Exibe a resposta da API no console
        Alert.alert("Bem vindo", response.data.message);
        navigation.navigate("HomePage", {
          user: response.data.user.cpf,
          nameUser: response.data.user.name,
        }); // Vá para a próxima tela após Login
      })
      .catch((error) => {
        Alert.alert("Erro", error.response.data.error);
      });
  };

  const handleSignupRedirect = () => {
    navigation.navigate("SignupScreen");
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder="Email"
        placeholderTextColor="#fff"
        onChangeText={(text) => setCredentials({ ...credentials, email: text })}
        value={credentials.email}
      />
      <TextInput
        style={styles.input}
        placeholder="Password"
        placeholderTextColor="#fff"
        onChangeText={(text) =>
          setCredentials({ ...credentials, password: text })
        }
        value={credentials.password}
        secureTextEntry
      />
      <View style={styles.buttonContainer}>
        <View style={styles.buttonWrapper}>
          <Button title="Login" onPress={handleLogin} color="green" />
        </View>
        <View style={styles.spacing} />
        <View style={styles.buttonWrapper}>
          <Button
            title="Cadastre-se"
            onPress={handleSignupRedirect}
            color="#0f3b50"
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#084d6e",
  },
  input: {
    width: "80%",
    marginBottom: 10,
    padding: 15,
    color: "#fff",
    borderWidth: 1,
    borderColor: "#fff",
    borderRadius: 10,
  },
  buttonContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonWrapper: {
    width: '100%',
    borderRadius: 10,
    overflow: 'hidden', 
  },
  spacing: {
    height: 10,
  },
});

export default HomeScreen;
