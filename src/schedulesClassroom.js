import React, { useEffect, useState } from "react";
import { View, StyleSheet, Text, FlatList, Alert } from "react-native";
import { Ionicons } from '@expo/vector-icons';
import WeekSelector from "./components/weekSelector";
import api from "./axios/axios";

const SchedulesClassroom = ({ route }) => {
  const { classroom , user} = route.params;
  const [schedulesByDayAndTimeRange, setSchedulesByDayAndTimeRange] = useState({});
  const [countDeleteSchedule, setCountDeleteSchedule] = useState(0);

  useEffect(() => {
    const fetchSchedulesClassroom = async () => {
        try {
          const response = await api.getSchedulesByIdClassroomRanges(classroom);
          setSchedulesByDayAndTimeRange(response.data.schedulesByDayAndTimeRange);
        } catch (error) {
          console.error("Erro ao obter reservas:", error);
        }
      };
    fetchSchedulesClassroom();
  }, [countDeleteSchedule]);

  // Função para lidar com a seleção da semana no WeekSelector
  const handleSelectWeek = async (weekStart,weekEnd) => {
    try {
      const response = await api.getSchedulesByIdClassroomRanges(classroom, weekStart,weekEnd);
      setSchedulesByDayAndTimeRange(response.data.schedulesByDayAndTimeRange);
    } catch (error) {
      console.error("Erro ao obter reservas:", error);
    }
    console.log(weekStart+" "+weekEnd)
  };

  const renderScheduleItem = ({ item }) => (
    <View style={styles.scheduleContainer}>
        <Text style={[styles.scheduleText, styles.scheduleTextLeft]}>{item.id}-Docente: {item.userName}</Text>
        <Text style={[styles.scheduleText, styles.scheduleTextLeft]}>{item.timeStart} - {item.timeEnd}</Text>
        {user === item.user && (
            <Ionicons
                name="trash-outline"
                size={24}
                color="#fff"
                style={styles.trashIcon}
                onPress={() => handleDeleteSchedule(item.id)}
            />
        )}
    </View>
  );

  const handleDeleteSchedule = async (scheduleId) => {
    try {
      await api.deleteSchedule(scheduleId);
      setCountDeleteSchedule(countDeleteSchedule+1); 
    } catch (error) {
        Alert.alert("Erro", error.response.data.error);
    }
  };

  return (
    <View style={styles.container}>
      {/* Inclui o WeekSelector no início da tela */}
      <WeekSelector onSelectWeek={handleSelectWeek} />
      <FlatList
        data={Object.entries(schedulesByDayAndTimeRange)}
        keyExtractor={(item) => item[0]}
        renderItem={({ item }) => (
          <View style={styles.dayContainer}>
            <Text style={styles.dayTitle}>{item[0]}</Text>
            {Object.entries(item[1]).map(([timeRange, schedules]) => (
              <View key={timeRange}>
                <Text style={styles.timeRangeTitle}>{timeRange}</Text>
                {schedules.length > 0 ? (
                  <FlatList
                    data={schedules}
                    keyExtractor={(schedule) => schedule.id.toString()}
                    renderItem={renderScheduleItem}
                  />
                ) : (
                  <Text style={styles.noScheduleText}>Nenhum agendamento para este horário</Text>
                )}
              </View>
            ))}
          </View>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  dayContainer: {
    marginBottom: 20,
  },
  dayTitle: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 10,
  },
  timeRangeTitle: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 5,
  },
  scheduleText: {
    color: "#fff",
    textAlign: "right", // Alinha o texto à direita
  },
  scheduleTextLeft: {
    textAlign: "left", // Alinha o texto à esquerda
  },
  scheduleContainer: {
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 10,
    padding: 10,
    marginBottom: 10,
    backgroundColor: "#084d6e",
    flexDirection: "row", // Para alinhar os itens horizontalmente
    justifyContent: "space-between", // Para espaçar os itens igualmente ao longo do container
    alignItems: "center", // Para alinhar os itens verticalmente
  },
  trashIcon: {
    marginLeft: 10, // Adiciona um espaço à esquerda do ícone
  },
  noScheduleText: {
    fontStyle: 'italic',
    color: 'gray',
  },
});

export default SchedulesClassroom;